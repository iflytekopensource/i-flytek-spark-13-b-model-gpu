本仓提供星火开源13B大模型GPU版本对应的预训练权重、微调权重和词表文件用于训练/微调/推理。

预训练权重：
iFlytekSpark_13B_base_fp32

微调权重：
iFlytekSpark_13B_chat_fp32

Tokenizer：
Tokenizer
